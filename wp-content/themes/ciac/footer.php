					<footer id="footer" role="contentinfo">
                        <div id="footer-1">
                            <img src="/wp-content/uploads/2016/02/CIA-pro-logo-grey.png" alt="<?php bloginfo( 'name'); ?>" >
                            <div id="address">1946 Campus Drive &middot; Hyde Park, NY 12538-1499 &middot;  <a href="tel:8459054423">845.905.4423</a></div>
                            <div id="footer-social">
                                <?php if( '' != get_theme_mod( 'facebook_link' ) ) { ?>
								<a href="<?php echo get_theme_mod( 'facebook_link' );?>" target="_blank"><img src="<?php echo esc_url( home_url( '/' ) ); ?>/wp-content/themes/ciac/library/images/footer-facebook.png" alt="Facebook"/></a>
								<?php } ?>
                                <?php if( '' != get_theme_mod( 'twitter_link' ) ) { ?>
                                <a href="<?php echo get_theme_mod( 'twitter_link' );?>" target="_blank"><img src="<?php echo esc_url( home_url( '/' ) ); ?>/wp-content/themes/ciac/library/images/footer-twitter.png" alt="Twitter"/></a> 
                                <?php } ?>
                                <?php if( '' != get_theme_mod( 'pinterest_link' ) ) { ?>
                                <a href="<?php echo get_theme_mod( 'pinterest_link' );?>" target="_blank"><img src="<?php echo esc_url( home_url( '/' ) ); ?>/wp-content/themes/ciac/library/images/social-pinterest.png" alt="Pinterest"/></a>
                                <?php } ?>
                                <?php if( '' != get_theme_mod( 'youtube_link' ) ) { ?>
                                <a href="<?php echo get_theme_mod( 'youtube_link' );?>" target="_blank"><img src="<?php echo esc_url( home_url( '/' ) ); ?>/wp-content/themes/ciac/library/images/social-youtube.png" alt="YouTube"/></a>
                              <?php } ?>
                              
                              <?php if( '' != get_theme_mod( 'linkedin_link' ) ) { ?>
		                        <a id="head_li" href="<?php echo get_theme_mod( 'linkedin_link' );?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/library/images/footer-linkedin.png" alt="Linkedin Link"/></a>
		                        <?php } ?>
                              
                             </div> <!-- end #footer-social -->	
                        </div>
	        			<div id="footer-2">
							<div id="copyright">&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?>. All rights reserved.</div>
                            <div id="nav-bottom">
								<nav role="navigation">
		    					<?php kickoff_footer_links(); ?>
		    					</nav>
		    				</div>
                        </div> <!-- end #footer-2 -->
					</footer> <!-- end #footer -->
				<!--</div>  end #container -->
		<!-- all js scripts are loaded in library/kickoff.php -->
	    <?php wp_footer(); ?>
</body>
</html> <!-- end page -->