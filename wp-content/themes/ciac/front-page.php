<?php

/* Template Name: Home Page */

get_header(); 
?>
	<div id="content-wrapper">
	<?php
     if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

        <!-- WELCOME SECTION -->
     <span class="anchor" id="welcome-section-link"></span>
     <section id="welcome-section">
          <div class="row">    
            <div class="medium-12 columns">
                     <h2><?php echo get_field('welcome_header');  ?></h2>
                     <?php echo get_field('welcome_message');  ?>
             </div>
          </div>
     </section>

    <!-- ABOUT SECTION -->
	<span class="anchor" id="about-section-link"></span>
    <section id="about-section">
      <div class="row">
		<div class="medium-2 columns right">
		<?php $mainimage = get_field('about_main_image');  ?>
		  <img id="about-main-img" src="<?php echo $mainimage['url']; ?>" alt="<?php echo $mainimage['alt']; ?>">
		</div> <!-- end main right col -->
		<div class="medium-8 columns pull-2 right">      
			<div class="medium-12 columns">
			  <h2><?php echo get_field('about_heading');  ?></h2>
			  <div id="about-copy" class="columns">
			  	<?php echo get_field('about_copy');  ?>
				</div> <!-- end inner right col -->
			</div> <!-- end heading col -->
		</div> <!-- end main left col -->

		<div class="medium-2 columns left">
			<div id="about-content" class="medium-4 columns">
			  <?php $leftimage = get_field('about_left_image');  ?>
			    <a href="http://www.ciachef.edu/" target="_blank">
			    	<img id="about-left-img" src="<?php echo $leftimage['url']; ?>" alt="<?php echo $leftimage['alt']; ?>">
			    </a>  			
			</div> <!-- end inner left col -->
		</div>
      </div> 	<!-- end main row -->	
  </section>
             
     <!-- SERVICES SECTION -->
	<span class="anchor" id="services-section-link"></span> 
    <section id="services-section">
    <?php if (get_field('svc_heading')) { ?>
    <h3><?php echo get_field('svc_heading'); ?></h3>
        <div class="row">            
              <?php } ?>
      
                 <?php 
                 $services = get_field('svc_descriptions');
                 $x = 0;
                 foreach ($services as $svc) {
	                  echo "<div id='service-".$x."' class='service-tab medium-4 columns'>";
	                  echo "<a href='".$svc['svc_pagelink']."'><img src='".$svc['svc_icon']['url']."' alt='".$svc['svc_icon']['alt']."'></a>";
	                  echo "<h4><a href='".$svc['svc_pagelink']."'>".$svc['svc_heading']."</a></h4>";
	                  echo "<div class='service-description'>".$svc['svc_description']."</div>";
	                  echo "<div class='read-more'><a href='".$svc['svc_pagelink']."'>".$svc['svc_linktext']."</a></div>";	
	                  echo "</div>";
	                  $x++;
                 }		
             ?> 
         </div> <!-- end row -->
    </section>
    <style>
	    @media(min-width:58.75em){
		    #services-section{
			    background-image: url("<?php echo get_field("svc_background_image"); ?>");
		    }
		}
	    
    </style>
  
    <!----- TEAM SECTION -->
    <span class="anchor" id="team-section-link"></span>
    <div id="team-section" class="clearfix">
          <div class="row">
              <div class="medium-12 columns">
                <?php if (get_field('team_heading')) { ?>
                <h2><?php echo get_field('team_heading'); ?></h2>
                <?php } ?>
                <div id="team-carousel">
                    <div class="teamslideshow">  
                           <?php 
                            $teaminfo =  get_field('team_info');
                           foreach ($teaminfo as $teammbr) {
                                echo "<div class='team-member'>";
									echo "<div class='image-wrapper'>";
										echo "<img src='".$teammbr['teammbr_picture']['url']."' alt='".$teammbr['teammbr_name']."'>";
										echo "<div class='team-member-descr'>";
											echo "<h3>".$teammbr['teammbr_overlay_name']."</h3>";	
											echo "<p class='teammbr-qualification'>".$teammbr['teammbr_overlay_qualifications']."</p>";
											echo "<p class='teammbr-position'>".$teammbr['teammbr_overlay_position']."</p>";
											echo "<p class='teammbr-descr'>".$teammbr['teammbr_overlay_description']."</p>";
										echo "</div>";
									echo "</div>";
									echo "<h3>";
									if($teammbr["teammbr_linkedin_link"] != "" ){
										echo "<a class='linkedin-link' target='_blank' href='" . $teammbr["teammbr_linkedin_link"] ."'>";
									}
									echo $teammbr['teammbr_name'];
									if($teammbr["teammbr_linkedin_link"] != "" ){
										echo "</a>";
									}
									echo "</h3>";
								echo "<p class='teammbr-title'>".$teammbr['teammbr_title']."</p>";
								echo "</div>";													   									
							?>
                      <?php } ?>
                    </div>   <!-- end teamslideshow -->
                   
                    <a href="#" id="cycle-prev"><img src="<?php echo get_template_directory_uri(); ?>/library/images/arrow-prev-orange.png"></a> 
                    <a href="#" id="cycle-next"><img src="<?php echo get_template_directory_uri(); ?>/library/images/arrow-next-orange.png"></a>
                    
                 </div> <!-- end team-carousel --> 
                 
               </div> <!-- end medium-12 -->
            </div> <!-- end row -->
        </div> <!-- end team section -->
            
	<script>
           
           $(window).load(function initTeamCarousel() {
             /* var width = $(window).width(); 
              if ( width < 641 ) { */
              $('.teamslideshow').cycle({
                  fx: 'carousel',
                  slides: '> div',
                  next: '#cycle-next',
                  prev: '#cycle-prev',
                  timeout: 0,
                  carouselVisible: 4,
              }); 
              /*}
              else if ( width < 771 ) {
              $('.teamslideshow').cycle({
                  fx: 'carousel',
                  slides: '> div',
                  next: '#cycle-next',
                  prev: '#cycle-prev',
                  carouselVisible: 2
              }); 
              }
              
              else if ( width < 1001 ) {
              $('.teamslideshow').cycle({
                  fx: 'carousel',
                  slides: '> div',
                  next: '#cycle-next',
                  prev: '#cycle-prev',
                  carouselVisible: 3
              }); 
              }
              else {
                  $('.teamslideshow').cycle({
                  fx: 'carousel',
                  slides: '> div',
                  next: '#cycle-next',
                  prev: '#cycle-prev',
                  carouselVisible: 4
                }); 
          }*/
          });
          
           </script>       


	<?php   get_template_part("partials/content","contact"); ?>           
                                    
      <!-- CLIENTS SECTION -->
      
        <?php $bgimg =  get_field('clients_bg_img'); ?>
        <span class="anchor" id="clients-section-link"></span>
         <section id="clients-section" class="clearfix">	
           <div class="row">
              <div class="medium-12  columns">
              <?php if (get_field('clients_heading')) { ?>
              <h2><?php echo get_field('clients_heading'); ?></h2>
              <?php } ?>
              
              <?php if(get_field('client_logos')): ?>
                <div id="clients-carousel">                          
                      
               <?php 
               $logos = get_field('client_logos');
               // split into two rows
               if(count($logos) > 4){
	               $arraysize = count($logos);
				   $splitsize = $arraysize / 2;
               }else{
	               $splitsize = 4;
               }
               $i = 0;
                   foreach ($logos as $logo)
                    {
                       if ($splitsize > $i){
                            $clientrow1 .= "<div class='client-logo-wrapper'>";
                            if($logo["client_link"] != ""){
	                            $clientrow1 .= "<a href='".$logo["client_link"]."' target='_blank'>";
                            }
                            $clientrow1 .= "<img src='".$logo['client_logo']['url']."' alt='".$svc['client_logo']['alt']."'>";
                            if($logo["client_link"] != ""){
	                            $clientrow1 .= "</a>";
                            }
                            $clientrow1 .= "</div>";
                        }else{
	                        $clientrow2 .= "<div class='client-logo-wrapper'>";
                            if($logo["client_link"] != ""){
	                            $clientrow2 .= "<a href='".$logo["client_link"]."' target='_blank'>";
                            }
                            $clientrow2 .= "<img src='".$logo['client_logo']['url']."' alt='".$svc['client_logo']['alt']."'>";
                            if($logo["client_link"] != ""){
	                            $clientrow2 .= "</a>";
                            }
                            $clientrow2 .= "</div>";	                        
	                     }//end else`   
                    $i++;
                   }						 

                   echo "<div class='clientslideshow'><div id='clientslideshow1'>".$clientrow1."</div>";
                   if(isset($clientrow2)){
	                   echo "<div id='clientslideshow2'>".$clientrow2."</div></div>";
					}
               ?>   
               
               
             <?php endif; ?> 
                  <a href="#" id="cycle-prev-2"><img src="<?php echo get_template_directory_uri(); ?>/library/images/arrow-prev-white.png"></a> 
                  <a href="#" id="cycle-next-2"><img src="<?php echo get_template_directory_uri(); ?>/library/images/arrow-next-white.png"></a>
               </div> <!-- end  clients-carousel -->
             </div> <!-- end medium-12 --> 
           </div><!-- end .row -->
          </section> <!-- end clients section -->
		 <script>
		 /* row 1 carousel */
            // $(window).load(function initClientsCarousel1() {
            /*  var width = $(window).width(); 
                if ( width < 641 ) {
                    $('#clientslideshow1').cycle({
                        fx: 'carousel',
                        slides: '> div',
                        next: '#cycle-next-2',
                        prev: '#cycle-prev-2',
                        timeout: 0,
                        carouselVisible: 4
                    }); 
                    $('#clientslideshow2').cycle({
                        fx: 'carousel',
                        slides: '> div',
                        next: '#cycle-next-2',
                        prev: '#cycle-prev-2',
                        timeout: 0,
                        carouselVisible: 4
                    }); 
             }//end if
             else {
                $('#clientslideshow1').cycle({
                    fx: 'carousel',
                    slides: '> div',
                    next: '#cycle-next-2',
                    prev: '#cycle-prev-2',
                    carouselVisible: 4, 
                    allowWrap: false /* so the images aren't duplicated on large screens */
               /*  }); 
               $('#clientslideshow2').cycle({
                        fx: 'carousel',
                        slides: '> div',
                        next: '#cycle-next-2',
                        prev: '#cycle-prev-2',
                        timeout: 0,
                        carouselVisible: 4, 
                        allowWrap: false /* so the images aren't duplicated on large screens */
                 /*   }); 
            }//end else
             
                */
                /*}
                else if ( width < 761 ) {
                $('#clientslideshow1').cycle({
                    fx: 'carousel',
                    slides: '> div',
                    next: '#cycle-next-2',
                    prev: '#cycle-prev-2',
                    carouselVisible: 3
                }); 
                }
                else {
                    $('#clientslideshow1').cycle({
                    fx: 'carousel',
                    slides: '> div',
                    next: '#cycle-next-2',
                    prev: '#cycle-prev-2',
                    carouselVisible: 4
                }); 
                }
            });
            
            /* row 2 carousel */
             /* $(window).load(function initClientsCarousel2() {
                var width = $(window).width(); 
                if ( width < 641 ) {
                $('#clientslideshow2').cycle({
                    fx: 'carousel',
                    slides: '> div',
                    next: '#cycle-next-2',
                    prev: '#cycle-prev-2',
                    carouselVisible: 2
                }); 
                }
                else if ( width < 761 ) {
                $('#clientslideshow2').cycle({
                    fx: 'carousel',
                    slides: '> div',
                    next: '#cycle-next-2',
                    prev: '#cycle-prev-2',
                    carouselVisible: 3
                }); 
                }
                else {
                    $('#clientslideshow2').cycle({
                    fx: 'carousel',
                    slides: '> div',
                    next: '#cycle-next-2',
                    prev: '#cycle-prev-2',
                    carouselVisible: 4
                }); 
                }
            });					*/		
             </script>   


      <!----- STUDENTS SECTION -->

          <?php $studentsbgimg =  get_field('students_bg_img'); ?>
          <section class="students-section" data-parallax="scroll" data-bleed="50" data-image-src="<?php echo $studentsbgimg['url']; ?>">  
              <div class="row">
                  <div class="medium-12  columns">
                      <h2><?php echo get_field('students_heading'); ?></h2>
                  </div>
              </div>                  
              <div class="row">
                  <div class="medium-6 columns">
                      <?php $studentsimage = get_field('students_image'); ?>
                     <img src="<?php echo $studentsimage['url']; ?>" alt="<?php echo $studentsimage['alt']; ?>">
                 </div>
                  <div class="medium-6 columns">                  
                      <div id="students-copy">
                      <?php echo get_field('students_copy');  ?>
                      </div>
                  </div>
              </div> 
          </section>
      
  
      <?php            
      endwhile; 
  endif;
  ?>
  </div>
     <script>   
     /*$(document).ready(function () {
        $(window).resize(function() {
          initClientsCarousel1();
          initClientsCarousel2();	
           intTeamCarousel();					
        });
      });*/
</script>   
<?php  get_footer();  ?>

