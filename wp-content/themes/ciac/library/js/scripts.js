/*
kickoff Scripts File

This file should contain any js scripts you want to add to the site.
Instead of calling it in the header or throwing it inside wp_head()
this file will be called automatically in the footer so as not to
slow the page load.

*/


// as the page loads, call these scripts
jQuery(document).ready(function($) {
	// (do not)load Foundation
	//jQuery(document).foundation();
	
	//Fix parallax scrolling issue for IE
	  
	  /*
        var $window = $(window);
		var scrollTime = 1.2;
		var scrollDistance = 170;
	
		$window.on("mousewheel DOMMouseScroll", function(event){
			//event.preventDefault();	
	
			var delta = event.originalEvent.wheelDelta/120 || -event.originalEvent.detail/3;
			var scrollTop = $window.scrollTop();
			var finalScroll = scrollTop - parseInt(delta*scrollDistance);
	
			TweenMax.to($window, scrollTime, {
				scrollTo : { y: finalScroll, autoKill:true },
					ease: Power1.easeOut,
					overwrite: 5							
				});
		});
    */
    
    var $html = $("body, html, document");
    //alert(window.navigator.userAgent);
    if (window.navigator.userAgent.indexOf("Windows NT 10.0") >= 0) {
        $('body').scroll(function (event, delta) {
            // disable native scroll
            if (event.preventDefault) {
                event.preventDefault();
            } else { // IE fix
                event.returnValue = false;
            };
            $html.stop().animate({
                scrollTop: $html.scrollTop() + (-delta * 500)
            }, 'slow');
        });
    }
    
    
    //hide services tab amd show while scrolling down
    $(".service-tab").hide();
    var $window = $(window);
    var tabs_visible = false;
    jQuery(window).scroll(function(){
        if($window.scrollTop() > 650 && !tabs_visible && $window.width() > 900){
	        jQuery(".service-tab").fadeIn("slow");
	        tabs_visible = true;
        }

        if($window.scrollTop() < 650 && tabs_visible && $window.width() > 940){
	        jQuery(".service-tab").fadeOut("medium");
	        tabs_visible = false;
        }
        
        if($window.scrollTop() > 450){
	        jQuery("#sticky-header").addClass("shadow");
        }else{
	        jQuery("#sticky-header").removeClass("shadow");
        }
	});
    
    
    
    $(".gfield_error").each(function(){
    	label = $(this).children('label').text();
    	
    	var PageName = window.location;
    	
	     if (label.length > 75) {
	        label = jQuery.trim(label).substring(0, 75) + "...";
	     }
	    ga('send', 'event', 'Gravity Forms - '+PageName, 'Form Field Errors', label)
	});
	
	var $html = $("body, html, document");
	/*alert(window.navigator.userAgent);
    if (window.navigator.userAgent.indexOf("Trident") >= 0) {
	    alert("IE");
        $('body').mousewheel(function (event, delta) {
            // disable native scroll
            if (event.preventDefault) {
                event.preventDefault();
            } else { // IE fix
                event.returnValue = false;
            };
            $html.stop().animate({
                scrollTop: $html.scrollTop() + (-delta * 500)
            }, 'slow');
        });
    }// end if	
   */

// add all your scripts here
	
});

/*! A fix for the iOS orientationchange zoom bug.
 Script by @scottjehl, rebound by @wilto.
 MIT License.
*/
(function(w){
	// This fix addresses an iOS bug, so return early if the UA claims it's something else.
	if( !( /iPhone|iPad|iPod/.test( navigator.platform ) && navigator.userAgent.indexOf( "AppleWebKit" ) > -1 ) ){ return; }
    var doc = w.document;
    if( !doc.querySelector ){ return; }
    var meta = doc.querySelector( "meta[name=viewport]" ),
        initialContent = meta && meta.getAttribute( "content" ),
        disabledZoom = initialContent + ",maximum-scale=1",
        enabledZoom = initialContent + ",maximum-scale=10",
        enabled = true,
		x, y, z, aig;
    if( !meta ){ return; }
    function restoreZoom(){
        meta.setAttribute( "content", enabledZoom );
        enabled = true; }
    function disableZoom(){
        meta.setAttribute( "content", disabledZoom );
        enabled = false; }
    function checkTilt( e ){
		aig = e.accelerationIncludingGravity;
		x = Math.abs( aig.x );
		y = Math.abs( aig.y );
		z = Math.abs( aig.z );
		// If portrait orientation and in one of the danger zones
        if( !w.orientation && ( x > 7 || ( ( z > 6 && y < 8 || z < 8 && y > 6 ) && x > 5 ) ) ){
			if( enabled ){ disableZoom(); } }
		else if( !enabled ){ restoreZoom(); } }
	w.addEventListener( "orientationchange", restoreZoom, false );
	w.addEventListener( "devicemotion", checkTilt, false );
})( this );


// click event to open and close mobile menu 

$(document).ready(function(){
	$('#menuopen').click(function(){
		$("#menuopen > i").toggleClass("fa-bars fa-times");
		$('#navmenu').slideToggle('medium');
	});
	
	$('#menu-main-nav li a').click(function(){
		$("#navmenu").hide();
		$("#menuopen > i").toggleClass("fa-bars fa-times");
	});
	
	
	
});