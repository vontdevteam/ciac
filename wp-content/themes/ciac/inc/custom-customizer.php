<?php
/*
* Custom Customizer options for ciac
*
*/
function ciac_customizer_register( $wp_customize ){
	$wp_customize->add_panel( 'ciac_theme_option', array(
	  	'priority' => 150,
	  	'capability' => 'edit_theme_options',
	  	'title' => __( 'CIAC Theme Options', 'ciac' ),
	  	) );
	/*
	* Section for Social Icons
	*
	*/
	$wp_customize->add_section('social_icons', array(
			'title' => __('Header Options', 'ciac'),
			'panel' => 'ciac_theme_option'
		));

	/*
	* Settings for site logo
	*
	*/
	$wp_customize->add_setting('site_logo', array(			
		  	'type' => 'theme_mod',
  		  	'sanitize_callback' => 'ciac_sanitize_url',
		));
	$wp_customize->add_control(
		new WP_Customize_Image_Control(
			$wp_customize,	
			'site_logo',
			array(
				'label' => __( 'Site Logo', 'ciac' ),
				'section' => 'social_icons',
				// 'settings' => 'confluence_settings',
				// 'type' => 'image'
			)
		)
	);


	/*
	* Settings for facebook link
	*
	*/
	$wp_customize->add_setting('facebook_link', array(			
		  	'type' => 'theme_mod',
  		  	'sanitize_callback' => 'ciac_sanitize_url',
		));
	$wp_customize->add_control('facebook_link', array(
			'label' => 'Facebook URL',
			'section' => 'social_icons',
			'settings' => 'facebook_link',
			'type'	=> 'url',
		));

	/*
	* Settings for facebook link
	*
	*/
	$wp_customize->add_setting('twitter_link', array(
			'type' => 'theme_mod',
			'sanitize_callback' => 'ciac_sanitize_url',
		));

	$wp_customize->add_control('twitter_link', array(
				'label' => __('Twitter URL', 'ciac'),
				'section' => 'social_icons',
				'settings' => 'twitter_link'
		));

	/*
	* Settings for Pinterest link
	*
	*/
	$wp_customize->add_setting('pinterest_link',array(
			'type' => 'theme_mod',
			'sanitize_callback' => 'ciac_sanitize_url',
		));
	$wp_customize->add_control('pinterest_link', array(
			'label' => __('Pinterest URL', 'ciac'),
			'settings' => 'pinterest_link',
			'section'	=> 'social_icons'
		));

	
	/*
	* Settings for YouTube link
	*
	*/
	$wp_customize->add_setting('youtube_link',array(
			'type' => 'theme_mod',
			'sanitize_callback' => 'ciac_sanitize_url',
		));
	$wp_customize->add_control('youtube_link', array(
			'label' => __('YouTube URL', 'ciac'),
			'settings' => 'youtube_link',
			'section'	=> 'social_icons'
		));
		
	/*
	* Settings for Linkedin link
	*
	*/
	$wp_customize->add_setting('linkedin_link',array(
			'type' => 'theme_mod',
			'sanitize_callback' => 'ciac_sanitize_url',
		));
	$wp_customize->add_control('linkedin_link', array(
			'label' => __('Linkedin URL', 'ciac'),
			'settings' => 'linkedin_link',
			'section'	=> 'social_icons'
		));

	
		
		
			
	/*
	* Section for footer
	*
	*/
	$wp_customize->add_section( 'footer_options', array(
			'title' => __('Footer Options', 'ciac'),
			'panel' => 'ciac_theme_option',
		));
	/*
	* Settings for copyright text
	*
	*/
	$wp_customize->add_setting( 'copyright_text', array(
			'type' => 'theme_mod',
			'sanitize_callback' => 'ciac_sanitize_html'
		));
	$wp_customize->add_control( 'copyright_text', array(
			'label' => __('Copyright Text', 'ciac'),
			'section' => 'footer_options',
			'settings' => 'copyright_text'
		));
}
add_action( 'customize_register', 'ciac_customizer_register' );


/*
* Sanitize call back for url
*
*/
function ciac_sanitize_url( $url ) {
	return esc_url_raw( $url );
}
/*
* Sanitize call back for 'html' type text inputs
*
*/
function ciac_sanitize_html( $html ) {
	return wp_filter_post_kses( $html );
}