<?php
/*
Template Name: Standard Page
*/
?>

<?php get_header(); ?>
			
			<div id="content">
			
				<div id="inner-content">
			
				    <div id="main" role="main">
					<div class="panel">
                    </div>
						<?php if (have_posts()) : while (have_posts()) : 
							the_post(); 
							
							$content_size = 12;
							if(have_rows('image_sidebar')){
								$content_size = 8;
							}
							?>
							<div id="content-wrapper" class="clearfix row">
								<div id="page-content" class="left small-12 medium-<?php echo $content_size; ?>">
									<?php the_content(); 
									?>
								</div>
								
								<?php if(have_rows('image_sidebar')){
									echo "<div id='image-sidebar' class='medium-4 right mobile-hide'>";
										 while ( have_rows('image_sidebar') ) : the_row();
										 	$image = get_sub_field("image");
										 		if(get_sub_field("image_link")){
											 		echo "<a target='_blank' href='".get_sub_field("image_link")."'>";
										 		}
											 		echo "<img src='".$image["url"]."' alt='".$image["alt"]."' />";
											 	if(get_sub_field("image_link")){
												 	echo "</a>";
											 	}
										 
										 endwhile;
									echo "</div>";
								} ?>
						</div>		
						<!-- Case Study Section -->	
							
						<?php
							if(get_field("case_study_content")){
								echo '<div id="case_study_section"';
							if(get_field("case_study_background_image")){
									$bg = get_field("case_study_background_image"); 
									echo 'data-parallax="scroll" data-image-src="'.  $bg["url"] .'"';
								 } ?>
							 >
								
								<div class="row">
									<div id="case_study_content" class="small-12">
										<h2><?php echo get_field("case_study_title"); ?></h2>
										<?php echo get_field("case_study_content"); ?>
									</div>
								</div>
							</div>								


						<?php }//end if case_study_content ?>
											
						<!-- End Case Study Section -->
											
					    <?php   get_template_part("partials/content","contact"); ?>
												
					<?php endwhile; endif; ?>							
					    					
    				</div> <!-- end #main -->
				    
				</div> <!-- end #inner-content -->
    
			</div> <!-- end #content -->

<?php get_footer(); ?>
