
<a id="contact"></a>
    <section class="contact-section">
           
         	<h2><?php echo get_field('contact_us_heading');  ?>  </h2>
            <p><?php echo get_field('contact_us_copy');  ?></p>
                        
			<?php 
			
			if ( function_exists( 'gravity_form' ) ) {
				$formid = get_field('form_id');
				 /* see parameter values at https://www.gravityhelp.com/documentation/article/embedding-a-form/ */
				 gravity_form( $formid, false, false, false, null, $ajax = true, 1, true );
				 }
			?>

    </section>
