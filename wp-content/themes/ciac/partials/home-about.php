    <a id="about"></a>
        <section class="about-section">
            <?php $mainimage = get_field('about_main_image');  ?>
            <img id="about-main-img" src="<?php echo $mainimage['url']; ?>" alt="<?php echo $mainimage['alt']; ?>">
            
         	<h2><?php echo get_field('about_heading');  ?>  </h2>
            <p><?php echo get_field('about_copy');  ?></p>
            
            <?php $leftimage = get_field('about_left_image');  ?>
            <img id="about-left-img" src="<?php echo $leftimage['url']; ?>" alt="<?php echo $leftimage['alt']; ?>">
            
        </section>
        
        
	
    <a id="services"></a>
        <section class="services-section">
        
       	<?php if (get_field('svc_heading')) { ?>
        <h2><?php echo get_field('svc_heading'); ?></h2>
        <?php } ?>

           <?php 
		   $services = get_field('svc_descriptions');
		   foreach ($services as $svc) {
			echo "<img src='".$svc['svc_icon']['url']."' alt='".$svc['svc_icon']['alt']."'>";
			echo "<h3>".$svc['svc_heading']."</h3>";
			echo "<p>".$svc['svc_description']."</p>";
			echo "<p><a href='".$svc['svc_pagelink']."'>".$svc['svc_linktext']."</a></p>";	
		   }		
	   ?>
        </section>