
<a id="team" ></a>
    <section class="team-section">
  		<?php if (get_field('team_heading')) { ?>
        <h2><?php echo get_field('team_heading'); ?></h2>
        <?php } ?>

       <?php 
		$teaminfo =  get_field('team_info');
	   foreach ($teaminfo as $teammbr) {
			echo "<img src='".$teammbr['teammbr_picture']['url']."' alt='".$svc['teammbr_picture']['alt']."'>";
			echo "<h3>".$teammbr['teammbr_name']."</h3>";
			echo "<p>".$teammbr['teammbr_title']."</p>";
		   }				   
	   ?>
       
    </section>