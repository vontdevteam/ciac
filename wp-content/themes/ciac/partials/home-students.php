
<a id="students"></a>
    <?php $studentsbgimg =  get_field('students_bg_img'); ?>
    <section class="students-section" style="background-image:url(<?php echo $studentsbgimg['url']; ?>)">                    
        <h2><?php echo get_field('students_heading'); ?></h2>
        <?php $studentsimage = get_field('students_image'); ?>
       <img src="<?php echo $studentsimage['url']; ?>" alt="<?php echo $studentsimage['alt']; ?>">
        <div id="students-copy">
         	<p><?php echo get_field('students_copy');  ?></p>
         </div>
    </section>