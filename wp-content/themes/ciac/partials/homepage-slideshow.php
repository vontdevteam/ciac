<?php 
	//get repeater field    
    $slides = get_field("slides");
    
    if(get_field("slides_random")){
	    shuffle($slides); //randomize slides 
	}    
	
	$slide = $slides[mt_rand(0,count($slides))];
	$temp = strpos($slide["slide_headline"], "<span>");
	$fade_caption = substr($slide["slide_headline"], 0, $temp);
	
	?>
    
    <div id="slideshow"  data-parallax="scroll" data-bleed="15" data-image-src="<?php echo $slide["slide_image"] ?>">
		<div class='caption'>
			<span class="fade"><?php echo $fade_caption ?></span>
		<?php //echo substr ($slide["slide_headline"], $temp); 
			
			echo "<span>tion</span>";
		?></div>
    </div>



	<div id="slide-preloader" style="display:none!important"> 
		<!-- loading all Slideshowimages for the browser to chache them to prevent loading flicker for Slideshow -->
		<?php 
			
			foreach($slides as $slide){
				echo "<img src='" . $slide["slide_image"] . "' />";
			}
			
			
		?>
		
	</div>

    <script>
	    var slides = <?php echo json_encode($slides); ?>;
	    var index = 0;
	    
	    function shuffle(array) {
		  var currentIndex = array.length, temporaryValue, randomIndex;
		
		  // While there remain elements to shuffle...
		  while (0 !== currentIndex) {
		
		    // Pick a remaining element...
		    randomIndex = Math.floor(Math.random() * currentIndex);
		    currentIndex -= 1;
		
		    // And swap it with the current element.
		    temporaryValue = array[currentIndex];
		    array[currentIndex] = array[randomIndex];
		    array[randomIndex] = temporaryValue;
		  }
		
		  return array;
		}
		
		
		//shuffle array
		slides = shuffle(slides);

		function nextSlide() {
			index = (index + 1) % slides.length;
			var slide = slides[index];	 
			
			var width = $( window ).width();
		
			if (width > 781){
				//$(".parallax-mirror:last > img").attr("src",slide['slide_image']); //replace parallax image
				$("#slideshow").css('background-image','none');
			}
			//else{
				//alert(slide['slide_image']);
				$(".parallax-mirror:last > img").attr("src",slide['slide_image']); //replace parallax image	    
				$("#slideshow").css('background-image', 'url(' + slide['slide_image'] + ')'); //replace bg image just in case parallax fails 
			//}
			
			var caption = slide["slide_headline"];
			var temp = caption.indexOf("<span>"); //find first span
			
			fade_caption = caption.substr(0, temp); //extract first part of caption to be faded in/out
			
			$(".caption > .fade").animate({'opacity': 0}, 10, function () { //fade in first part of caption
				$(".caption > .fade").html(fade_caption);
			}).animate({'opacity': 1}, 1000);
		}
		
		window.setInterval(function(){ // rotate trough slides 
			nextSlide();
		}, 5000);
		
    </script>