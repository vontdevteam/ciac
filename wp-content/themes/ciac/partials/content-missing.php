<article id="post-not-found" class="hentry">
  <header class="article-header">
    <h1><?php _e("Oops, Post Not Found!", "kickofftheme"); ?></h1>
  </header>
  <section class="entry-content">
    <p><?php _e("Uh Oh. Something is missing. Try double checking things.", "kickofftheme"); ?></p>
  </section>
  <footer class="article-footer">
      <p><?php _e("This is the error message in the partials/missing-content.php template.", "kickofftheme"); ?></p>
  </footer>
</article>