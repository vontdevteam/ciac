<a id="clients" ></a>
		<?php $bgimg =  get_field('clients_bg_img'); ?>
         <section class="clients-section" style="background-image:url(<?php echo $bgimg['url']; ?>)">	
  		<?php if (get_field('clients_heading')) { ?>
        <h2><?php echo get_field('clients_heading'); ?></h2>
        <?php } ?>
       <?php 
	   $logos = get_field('client_logos');
		   foreach ($logos as $logo) {
			echo "<img src='".$logo['client_logo']['url']."' alt='".$svc['client_logo']['alt']."'>";
		   }		
	   ?>
       
	</section>
    