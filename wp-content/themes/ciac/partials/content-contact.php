<!----- CONTACT SECTION ---->
<span class="anchor" id="contact-section-link"></span>
<section id="contact-section" class="contact-section clearfix"
	<?php if(is_front_page()){ echo  ' data-parallax="scroll" data-bleed="50" data-image-src="/wp-content/themes/ciac/library/images/pots_bg.png" '; } ?> 
		
	>
	<div class="row">
	  <div class=" medium-12  columns">     
	      <h2><?php echo get_field('contact_us_heading');  ?>  </h2>
	  </div>
	  </div>
	  <div class="row">
	      <div class="medium-6  columns">
	          <div id="contact-copy"><?php echo get_field('contact_us_copy');  ?></div>
	      </div>     
	      <div class="medium-6  columns">
		      
	      <?php 
	           gravity_form( 1, false, false, false, null, $ajax = true, 1, true );
	      ?>
	      <div id='leaf'></div> 
      </div> <!-- end columns -->
  </div> <!-- end row -->
</section> 

<!-- end contact-section -->