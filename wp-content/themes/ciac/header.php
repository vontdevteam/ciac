<!doctype html>

  <html class="no-js"  <?php language_attributes(); ?>>

	<head>
		<meta charset="utf-8">
		
		<!-- Force IE to use the latest rendering engine available -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<title><?php wp_title(''); ?></title>

		<!-- Mobile Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<meta name="google-site-verification" content="3K7LcSjd93luXOZEgtUzcnvmIOBeKN6p623PDuo0Yo4" />
		<!-- Icons & Favicons -->
		<link rel="icon" href="/favicon.png?v=2">
		<link href="/apple-icon-touch.png" rel="apple-touch-icon" />
		<!--[if IE]>
			<link rel="shortcut icon" href="/favicon.ico">
		<![endif]-->
		<meta name="msapplication-TileColor" content="#802325">
		<meta name="msapplication-TileImage" content="/win8-tile-icon.png">
		<meta name="theme-color" content="#802325">

		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
        
        <!-- include jQuery -->
        <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>-->
        <script type="text/javascript" src="//fast.fonts.net/jsapi/d8ea8e98-a0de-4115-9a09-3771c0685139.js"></script>
        <script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/latest/TweenMax.min.js"></script>
		<script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/latest/plugins/ScrollToPlugin.min.js"></script>
		
		<?php wp_head(); ?>
		<!-- include Cycle2 and carousel add on -->        
		<script src="<?php echo get_template_directory_uri(); ?>/library/js/jquery.cycle2.min.js"></script>
		<script src="<?php echo get_template_directory_uri(); ?>/library/js/jquery.cycle2.carousel.min.js"></script>
		<!-- Drop Google Analytics here -->
			<script>
			  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
			
			  ga('create', 'UA-77841277-1', 'auto');
			  ga('require', 'displayfeatures');
			  ga('send', 'pageview');
			
			</script>
		<!-- end analytics -->

	</head>

	<body <?php body_class(); ?>>
            
      <!-- BANNER -->
      
          <header role="banner">
	           	<div id="sticky-header" class="fixed clearfix">
	              <div id="inner-header" class="clearfix">                      
	                            
	                  <a class="logo" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php echo get_theme_mod( 'site_logo' ); ?>" alt="<?php bloginfo( 'name'); ?>" ></a>
	                
	                 <div id="menuopen" class="menu-open"><i class="fa fa-bars"></i></div>
	                  
	                  <div id="navmenu">
	                    <nav role="navigation">
	                    <?php kickoff_main_nav(); ?>
	                    </nav>
	                  </div> 
		                  <div id="head-social">
		                    <?php if( '' != get_theme_mod( 'facebook_link' ) ) { ?>
		                        <a id="head_fb" href="<?php echo get_theme_mod( 'facebook_link' );?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/library/images/social-f-dark.png" alt="Facebook"/></a>
		                        <?php } ?>
		                        <?php if( '' != get_theme_mod( 'twitter_link' ) ) { ?>
		                        <a id="head_tw" href="<?php echo get_theme_mod( 'twitter_link' );?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/library/images/social-twitter-dark.png" alt="Twitter"/></a> 
		                        <?php } ?>
		                        <?php if( '' != get_theme_mod( 'pinterest_link' ) ) { ?>
		                        <a id="head_yt" href="<?php echo get_theme_mod( 'pinterest_link' );?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/library/images/social-pinterest-dark.png" alt="Pinterest"/></a>
		                        <?php } ?>
		                        <?php if( '' != get_theme_mod( 'youtube_link' ) ) { ?>
		                        <a id="head_yt" href="<?php echo get_theme_mod( 'youtube_link' );?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/library/images/social-youtube-dark.png" alt="YouTube"/></a>
		                        <?php } ?>
		                        
		                        <?php if( '' != get_theme_mod( 'linkedin_link' ) ) { ?>
		                        <a id="head_li" href="<?php echo get_theme_mod( 'linkedin_link' );?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/library/images/linkedin.png" alt="Linkedin Link"/></a>
		                        <?php } ?>
		                 
		                </div> <!-- end #head-social -->
		              </div>   <!-- end #inner-header -->
				</div> <!-- end #sticky-header -->
              <div id="banner">
                    <?php 
                    if (is_front_page()) {
                   		get_template_part("partials/homepage", "slideshow");
                    }else{
	                    if (get_field('banner_image')) {
	                    	$bannerimg =  get_field('banner_image');
	                    	$bannerimg = $bannerimg['url'];
	                    }else{
		                   $bannerimg = "/wp-content/themes/ciac/library/images/banner.png"; 
	                    }
						echo "<img src='".$bannerimg."'>"; ?>
						<div class="banner-overlay"></div>
						<div id="banner-caption">
		                  <h1>
	                        <?php  
	                        if (get_field('banner_nontrans_text')) {
	                            echo get_field('banner_nontrans_text')."<span id='".bnrTextTrans."'>".get_field('banner_trans_text')."</span>";
	                        }
	                            else { get_the_title(); }
	                        ?>
		                  </h1>
		                  </div>
		                  <?php if(get_field("banner_overlay")){ ?>
			                  <div id="banner-overlay">
				                  <div id="banner-overlay-inner" class="row">
					                  <p><?php echo get_field("banner_overlay"); ?></p>
				                  </div>
			                  </div>
						<?php }//end if banner_overlay
					}
                    ?>              
              </div>   <!-- end banner -->
                    
            </header> <!-- end .header -->
