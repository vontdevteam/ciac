			<div id="content">

				<div id="inner-content" class="row">

				    <div id="main" class="large-6 push-6 medium-6 columns" role="main">

					   <?php include(locate_template( 'partials/loop-page.php')); ?>

    				</div> <!-- end #main -->

    				<?php get_sidebar(); ?>

				</div> <!-- end #inner-content -->

				<div id="portfolio-wrapper">

                    <div class="row">
                        <div class="large-12 columns">
            				<div class="stitch"></div>
            				<H2>M&A Advisory - Majority/Full Acquisitions</H2>
                        </div>
                    </div>

                    <div class="row">

                        <div id="cycle-prev-ma" class="cycle-prev"></div>
                        <div id="cycle-next-ma" class="cycle-next"></div>
                        <div id="carousel-ma" class="portfolio-carousel">

            				<?php
                    		    $query_options = array(
                            		'posts_per_page'  => -1,
                            		'offset'          => 0,
                            		'orderby'         => 'menu_order',
                            		'order'           => 'ASC',
                            		'post_type'       => array('portfolio'),
                            		'post_status'     => 'publish',
                            		'tax_query'       => array(
                            		                        array(
                            		                            'taxonomy' => 'portfolio_categories',
                            		                            'field' => 'slug',
                            		                            'terms' => 'ma-advisory'
                                                        ))
                            	);
                            	$portfolio_items = get_posts($query_options);

                            	$i = 1;
                            	foreach ( $portfolio_items as $post ) :

                                    setup_postdata( $post );
                                    $link = get_the_permalink();
                                    $title = get_the_title();
                                    $content = get_the_content();
                                    $fields = get_fields();
                                    $image = get_field('image');

                            	?>
                            		<img src="<?php echo $image; ?>" />
                            	<?php
                            	    $i++;
                            	endforeach;
                            	wp_reset_postdata();
                            ?>
                            <div class="clearfix"></div>
        				</div>
                    </div>

                    <div class="row">
                        <div class="large-12 columns">
            				<div class="stitch"></div>
            				<H2>Private Placements - Minority Investments</H2>
                        </div>
                    </div>

                    <div class="row">

                        <div id="cycle-prev-pp" class="cycle-prev"></div>
                        <div id="cycle-next-pp" class="cycle-next"></div>
                        <div id="carousel-pp" class="portfolio-carousel">

            				<?php
                    		    $query_options = array(
                            		'posts_per_page'  => -1,
                            		'offset'          => 0,
                            		'orderby'         => 'menu_order',
                            		'order'           => 'ASC',
                            		'post_type'       => array('portfolio'),
                            		'post_status'     => 'publish',
                            		'tax_query'       => array(
                            		                        array(
                            		                            'taxonomy' => 'portfolio_categories',
                            		                            'field' => 'slug',
                            		                            'terms' => 'private-placements'
                                                        ))
                            	);
                            	$portfolio_items = get_posts($query_options);

                            	$i = 1;
                            	foreach ( $portfolio_items as $post ) :

                                    setup_postdata( $post );
                                    $link = get_the_permalink();
                                    $title = get_the_title();
                                    $content = get_the_content();
                                    $fields = get_fields();
                                    $image = get_field('image');

                            	?>
                            		<img src="<?php echo $image; ?>" />
                            	<?php
                            	    $i++;
                            	endforeach;
                            	wp_reset_postdata();
                            ?>
                            <div class="clearfix"></div>
        				</div>
                    </div>

                    <div class="row">
                        <div class="large-12 columns">
            				<div class="stitch"></div>

            				<H2>Buy-Side Advisory</H2>
                        </div>
                    </div>

                    <div class="row">

                        <div id="cycle-prev-bs" class="cycle-prev"></div>
                        <div id="cycle-next-bs" class="cycle-next"></div>
                        <div id="carousel-bs" class="portfolio-carousel">

            				<?php
                    		    $query_options = array(
                            		'posts_per_page'  => -1,
                            		'offset'          => 0,
                            		'orderby'         => 'menu_order',
                            		'order'           => 'ASC',
                            		'post_type'       => array('portfolio'),
                            		'post_status'     => 'publish',
                            		'tax_query'       => array(
                            		                        array(
                            		                            'taxonomy' => 'portfolio_categories',
                            		                            'field' => 'slug',
                            		                            'terms' => 'buy-side-advisory'
                                                        ))
                            	);
                            	$portfolio_items = get_posts($query_options);

                            	$i = 1;
                            	foreach ( $portfolio_items as $post ) :

                                    setup_postdata( $post );
                                    $link = get_the_permalink();
                                    $title = get_the_title();
                                    $content = get_the_content();
                                    $fields = get_fields();
                                    $image = get_field('image');

                            	?>
                            		<img src="<?php echo $image; ?>" />
                            	<?php
                            	    $i++;
                            	endforeach;
                            	wp_reset_postdata();
                            ?>
                            <div class="clearfix"></div>
        				</div>
        				 <div class="clearfix"></div>
                    </div>


				</div> <!-- end #portfolio-wrapper -->

			</div> <!-- end #content -->

			<script>
    			jQuery(document).ready(function($) {

                    function initCycle() {

                        var width = jQuery(document).width(); // Getting the width and checking my layout
                        //alert('width: ' + width);

                        if ( width < 800 ) {
                            jQuery('#carousel-ma').cycle({
                                fx: 'carousel',
                                speed: 600,
                                manualSpeed: 100,
                                next: '#cycle-next-ma',
                                prev: '#cycle-prev-ma',
                                carouselVisible: 2,
                                allowWrap: true
                            });
                            jQuery('#carousel-pp').cycle({
                                fx: 'carousel',
                                speed: 600,
                                manualSpeed: 100,
                                next: '#cycle-next-pp',
                                prev: '#cycle-prev-pp',
                                carouselVisible: 2,
                                allowWrap: true
                            });
                            jQuery('#carousel-bs').cycle({
                                fx: 'carousel',
                                speed: 600,
                                manualSpeed: 100,
                                next: '#cycle-next-bs',
                                prev: '#cycle-prev-bs',
                                carouselVisible: 2,
                                allowWrap: true
                            });
                        } else if ( width < 920 ) {
                            jQuery('#carousel-ma').cycle({
                                fx: 'carousel',
                                speed: 600,
                                manualSpeed: 100,
                                next: '#cycle-next-ma',
                                prev: '#cycle-prev-ma',
                                carouselVisible: 3,
                                allowWrap: true
                            });
                            jQuery('#carousel-pp').cycle({
                                fx: 'carousel',
                                speed: 600,
                                manualSpeed: 100,
                                next: '#cycle-next-pp',
                                prev: '#cycle-prev-pp',
                                carouselVisible: 3,
                                allowWrap: true
                            });
                            jQuery('#carousel-bs').cycle({
                                fx: 'carousel',
                                speed: 600,
                                manualSpeed: 100,
                                next: '#cycle-next-bs',
                                prev: '#cycle-prev-bs',
                                carouselVisible: 3,
                                allowWrap: true
                            });
                        } else {
                            jQuery('#carousel-ma').cycle({
                                fx: 'carousel',
                                speed: 600,
                                manualSpeed: 100,
                                next: '#cycle-next-ma',
                                prev: '#cycle-prev-ma',
                                carouselVisible: 4,
                                allowWrap: true
                            });
                            jQuery('#carousel-pp').cycle({
                                fx: 'carousel',
                                speed: 600,
                                manualSpeed: 100,
                                next: '#cycle-next-pp',
                                prev: '#cycle-prev-pp',
                                carouselVisible: 4,
                                allowWrap: true
                            });
                            jQuery('#carousel-bs').cycle({
                                fx: 'carousel',
                                speed: 600,
                                manualSpeed: 100,
                                next: '#cycle-next-bs',
                                prev: '#cycle-prev-bs',
                                carouselVisible: 4,
                                allowWrap: true,
                                'cycle-timeout' : 0
                            });
                        }
                        // console.log('Init Mobile');
                    }

                    initCycle();
                    jQuery('#carousel-ma').cycle('pause');
                    jQuery('#carousel-pp').cycle('pause');
                    jQuery('#carousel-bs').cycle('pause');

                    function reinit_cycle() {
                    //alert('reinit_cycle');
                        var width = jQuery(document).width(); // Checking size again after window resize
                        if (( width < 800 )) {
                            $('#carousel-ma').cycle('destroy');
                            $('#carousel-pp').cycle('destroy');
                            $('#carousel-bs').cycle('destroy');
                            reinitCycle(2);
                        } else if (( width < 920 )) {
                            $('#carousel-ma').cycle('destroy');
                            $('#carousel-pp').cycle('destroy');
                            $('#carousel-bs').cycle('destroy');
                            reinitCycle(3);
                        } else  {
                            $('#carousel-ma').cycle('destroy');
                            $('#carousel-pp').cycle('destroy');
                            $('#carousel-bs').cycle('destroy');
                            reinitCycle(4);
                        }
                    }

                    function reinitCycle(visibleSlides) {
                    //alert(visibleSlides + 'visibleSlides');
                        jQuery('#carousel-ma').cycle({
                            fx: 'carousel',
                            speed: 600,
                            manualSpeed: 100,
                            next: '#cycle-next-ma',
                            prev: '#cycle-prev-ma',
                            carouselVisible: visibleSlides,
                            allowWrap: true
                        });
                        jQuery('#carousel-pp').cycle({
                            fx: 'carousel',
                            speed: 600,
                            manualSpeed: 100,
                            next: '#cycle-next-pp',
                            prev: '#cycle-prev-pp',
                            carouselVisible: visibleSlides,
                            allowWrap: true
                        });
                        jQuery('#carousel-bs').cycle({
                            fx: 'carousel',
                            speed: 600,
                            manualSpeed: 100,
                            next: '#cycle-next-bs',
                            prev: '#cycle-prev-bs',
                            carouselVisible: visibleSlides,
                            allowWrap: true
                        });
                        jQuery('#carousel-ma').cycle('pause');
                        jQuery('#carousel-pp').cycle('pause');
                        jQuery('#carousel-bs').cycle('pause');
                    }

                    var reinitTimer;
                    $(window).resize(function() {
                        clearTimeout(reinitTimer);
                        reinitTimer = setTimeout(reinit_cycle, 200);
                    });

                });

        </script>


<?php get_footer(); ?>